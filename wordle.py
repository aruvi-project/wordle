import requests as rq
import random
DEBUG = True

class WordleGame:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name:str):
        
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleGame.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleGame.creat_url, json=creat_dict)
        self.correct_place = {}
        self.could_be_anywhere = []
        self.could_be_nowhere = []
        self.choices = [w for w in WordleGame.words[:] if is_unique(w)]
        random.shuffle(self.choices)
        
    
    def classify_response(self, word:str, feedback:str):
        for index, letter in enumerate(word):
            if feedback[index] == 'G':
                self.correct_place[index] = letter
                if letter not in self.could_be_anywhere:
                    self.could_be_anywhere.append(letter)
            elif feedback[index] == 'Y':
                if letter not in self.could_be_anywhere:
                    self.could_be_anywhere.append(letter)
            else:
                self.could_be_nowhere.append(letter)
                
                
            
    

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleGame.guess_url, json=guess)
            rj = response.json()
            
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']
        max_attempts = 6
        curr_attempts = 0
        while not won and curr_attempts < max_attempts:
            if DEBUG:
                print(choice, right)
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
            curr_attempts += 1
            
        print("Word is", choice, "found in", len(tries), "attempts")

    def update(self, choice: str, right: str):
        def segregating_choices(choice: str, right: str, word:str):
            self.classify_response(choice, right)
            for i in self.correct_place:
                if self.correct_place[i] != word[i]:
                    return False
                else:
                    for i in word:
                        if i in self.could_be_nowhere:
                            return False
            return True
        def common(choice: str, word: str):
            return len(set(choice) & set(word))
        self.choices = [w for w in self.choices if segregating_choices(choice, right, w)]



game = WordleGame("Yashaswini")
game.play()
